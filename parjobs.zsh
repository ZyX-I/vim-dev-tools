autoload colors && colors
typeset -g STOPCOLOR="\e[${color[magenta]}m"
typeset -g ADDCOLOR=""
typeset -g RUNCOLOR="\e[${color[green]}m"
typeset -g RINFCOLOR="\e[${color[yellow]}m"
typeset -g HALTCOLOR="\e[${color[red]}m"
typeset -g Q="\e[0m"
typeset -gi CURJOB=0
typeset -gi JOBNUM=0
typeset -gi RUNNING=0
typeset -gi MAXJOBS=$(cat /proc/cpuinfo | grep '^processor' | tail -n 1 | \
                                          grep -o '[0-9]$')
(( MAXJOBS++ ))
typeset -gA JOB_PIDS
typeset -gi PRINTSTOP=1
typeset -gi PRINTNEW=1
typeset -gi PRINTRUN=1
typeset -gi PRINTSTATUS=0

function printjob()
{
    echo -n ${(P)1}
}
function testchildren()
{
    setopt rematchpcre
    local -i pid
    local -A RUNNING_JOBS
    local state
    for state in ${jobstates} ; do
        if [[ $state =~ '^[^:]+:.?:(\d+)' ]] ; then
            RUNNING_JOBS[${match[1]}]=1
        fi
    done
    for pid in ${(k)JOB_PIDS} ; do
        if [[ -z ${RUNNING_JOBS[$pid]} ]] ; then
            (( PRINTSTOP )) && {
                echo -n "${STOPCOLOR}Finished job "
                printjob ${JOB_PIDS[$pid]}
                echo $Q
            }
            unset "JOB_PIDS[$pid]"
            (( RUNNING-- ))
        fi
    done
    (( PRINTSTATUS )) &&
        echo "${RINFCOLOR}Jobs running: ${RUNNING}$Q"
    runjobs
}
function onstopjob()
{
    testchildren
}
function addjob()
{
    (( JOBNUM++ ))
    local -r JVAR=JOB_$JOBNUM
    typeset -ga $JVAR

    set -A $JVAR $@
    (( PRINTNEW )) && {
        echo -n "${ADDCOLOR}Added job ${JOBNUM}: "
        printjob $JVAR
        echo $Q
    }
    runjobs
}
function runjobs()
{
    while (( RUNNING<MAXJOBS && CURJOB<JOBNUM )) ; do
        runjob
    done
}
function runjob()
{
    (( RUNNING==MAXJOBS || CURJOB>=JOBNUM )) && return
    (( CURJOB++ ))
    local -r JVAR=JOB_$CURJOB
    [[ -z "${(P)JVAR}" ]] && return 1

    (( PRINTRUN )) && {
        echo -n "${RUNCOLOR}Running job number ${CURJOB}: "
        printjob $JVAR
        echo $Q
    }

    ( trap 'true' USR1 ; ${(P)JVAR} &>/dev/null ) &
    JOB_PIDS[$!]=$JVAR
    (( RUNNING++ ))
}
function stopjobs()
{
    echo "${HALTCOLOR}Stopping jobs$Q"
    (( JOBNUM=0 ))
    function addjob()
    {
    }
}
function waitjobs()
{
    while (( CURJOB<JOBNUM )) ; do
        runjobs
        sleep 2s
        testchildren
    done

    local line
    while (( RUNNING )) ; do
        sleep 5s
        testchildren
    done
}
function printrunning()
{
    for pid in ${(k)JOB_PIDS} ; do
        echo -n "${RINFCOLOR}${pid}\t"
        printjob ${JOB_PIDS[$pid]}
        echo $Q
    done
}
trap 'onstopjob' CHLD
trap 'printrunning' USR1
trap 'stopjobs' USR2
