#!/bin/zsh
local -a DIRS
local -i COMMIT=1
local -r CMSG="Updated list of files in addon-info.txt"
if [[ $1 == "--no-commit" ]] ; then
    shift
    COMMIT=0
fi
if (( $# )) ; then
    DIRS=( $@:a )
else
    DIRS=( ${0:a:h:h}/*(/) )
fi
for f in $DIRS ; do
    AIF=$f/$f:t-addon-info.txt
    test -e $AIF || continue
    (
        cd $f
        FILES=( ${(ps.\0.)"$(hg locate -0 \*/\*)"} )
        FILES=( \"${^FILES}\" )
        STR=$'    "files": [\n        '"${(pj.,\n        .)FILES}"$'\n    ],' \
            perl -p -i -e 'if(/^    "files"/../^    \],/) { undef $_; next } s/^}$/$ENV{"STR"}\n}/' $AIF
        (( COMMIT )) && hg commit $AIF -m $CMSG
    )
done
return 0
