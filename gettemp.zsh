typeset -x TMPDIR
function gettemp()
{
    emulate -L zsh
    local -r TMPBASE=${TMPBASE:=/tmp}
    local -r PREFIX=$1
    local -i I=0
    while true ; do
        TMPDIR="$TMPBASE/${PREFIX}-$I"
        if test -d $TMPDIR ; then
            if test -e $TMPDIR/pid ; then
                PID=$(< $TMPDIR/pid)
                if test -d /proc/$PID &&
                   test -e $TMPDIR/cmdline &&
                   diff -a $TMPDIR/cmdline /proc/$PID/cmdline
                then
                    (( I++ ))
                    continue
                fi
            fi
            rm -rf $TMPDIR
        fi
        break
    done
    mkdir -p $TMPDIR
    echo $$ > $TMPDIR/pid
    < /proc/$$/cmdline > $TMPDIR/cmdline
}
gettemp $@
trap "rm -f ${(q)TMPDIR}/pid" EXIT
