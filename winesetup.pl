#!/usr/bin/perl
use strict;
use warnings;
use File::Path;
use File::Glob;
use File::Basename;
use File::Copy;
use LWP::Simple;
use WWW::Mechanize;
use HTML::TreeBuilder;
use HTTP::Status;

sub globescape($) {
    (my $r=shift)=~s/([\[\]*?{}~\\])/\\$1/;
    $r;
}

my $stufroot = $ENV{"STUFDIR"} || File::Basename::dirname(Cwd::realpath($0));
$ENV{"TMPDIR"}=$ENV{"TMPDIR"} || $ENV{"HOME"}."/tmp/build/vim";
$ENV{"WINEPREFIX"}=$ENV{"HOME"}."/.wine" unless defined $ENV{"WINEPREFIX"};
my $wineprefix=$ENV{"WINEPREFIX"};
my $ewineprefix=globescape $wineprefix;
my $c=$wineprefix."/dosdevices/c:";
my $ec=globescape $c;
my $vim="$c/vim";
$\="\n";

my $skipuntil=shift @ARGV;
my $stopat=shift @ARGV;

my %libs;
my $mech=WWW::Mechanize->new(autocheck => 1);

sub run(@) {
    print "Running @_";
    (system @_)==0;
}
sub download($$) {
    my ($url, $target)=@_;
    print "Downloading $url to $target";
    run curl => $url => "-L", "-o", $target;
}
sub dlUncompress($$) {
    my $url=shift;
    my $target=shift;
    my $arch="$c/archive.archive";
    print "Uncompressing $url to $target";
    download $url => $arch
        or return 0;
    run "7z" => x => $arch => "-o$target", "-y"
        or return 0;
    unlink $arch;
    1;
}
sub convertToWinePath($) {
    my $path=shift;
    my $P;
    open $P, "-|", winepath => "-w", $path;
    $path=join "", <$P>;
    close $P;
    $path=~s/\r?\n$//;
    $path;
}
sub installMSI($) {
    my $url=shift;
    print "Installing $url using msiexec";
    my $target="$c/installer.msi";
    download $url => $target
        or return 0;
    run msiexec => "/q", "/a", $target
        or return 0;
    unlink $target;
    1;
}
sub postdll(@) {
    print "Copying DLLs: @_";
    local $_;
    for (@_) {
        File::Copy::copy($_, $vim)
            or return 0;
    }
    scalar @_;
}
sub rmTree($) {
    my $target=shift;
    print "Removing $target";
    File::Path::remove_tree($target);
}
sub addpath($) {
    my $path=convertToWinePath shift;
    print "Adding $path to %PATH%";
    run wine => cmd => "/c", 'echo %PATH%>C:\path.dat'
        or return 0;
    -r "$c/path.dat" or return 0;
    my $P;
    open $P, "<", "$c/path.dat"
        or return 0;
    my $pathval=<$P>;
    close $P;
    unlink "$c/path.dat";
    $pathval=~s/\r?\n//;
    $pathval=~s/;*$/;$path/;
    $pathval=~s/\\/\\\\/g;
    my $REG;
    open $REG, ">", "$c/path.reg"
        or return 0;
    local $\="\r\n";
    print $REG "REGEDIT4";
    print $REG "";
    print $REG '[HKEY_LOCAL_MACHINE\System\CurrentControlSet'.
                                        '\Control\Session Manager\Environment]';
    print $REG '"PATH"="'.$pathval.'"';
    close $REG;
    run wine => regedit => 'C:\path.reg'
        or return 0;
    unlink "$c/path.reg";
    1;
}
sub compareVersions($$) {
    my $v=shift;
    my $ov=shift;
    my @p=split /\D/, $v;
    my @op=split /\D/, $ov;
    while(@p) {
        my $comp=-(((shift@p)||0)<=>((shift@op)||0));
        if($comp != 0) {
            return $comp;
        }
    }
}
sub pyURL($) {
    my $py=shift;
    return "http://python.org/ftp/python/$py/python-$py.msi";
}
sub installPython($) {
    my $py=shift;
    my $pythonurl=pyURL($py);
    installMSI($pythonurl)
        or return 0;
    (my $pyv=$py)=~s/\.//g;
    $pyv=~s/^(..).*$/$1/;
    addpath "$c/Python$pyv";
}
sub addLib($) {
    my $lib=shift;
    my $ulib=uc $lib;
    my $ver=$libs{$lib};
    (my $truncver=$ver)=~s/^(\d+(?:\.\d+)?).*/$1/;
    $truncver=~s/\.//g;
    map {undef $ENV{$_}} grep(/^\Q$ulib/, keys %ENV);
    $ENV{$ulib."_VER_LONG"}=$ver;
    $ENV{$ulib."_VER"}=$truncver;

       if($lib=~/^python/) { $ENV{$ulib}="$c/Python$truncver";
                             $ENV{$ulib."INC"}="-I$ENV{$ulib}/include"; }
    elsif($lib eq "perl")  { $ENV{$ulib}="$c/strawberry/perl";
                             $ENV{$ulib."LIBS"}="$ENV{$ulib}/lib/CORE"; }
    elsif($lib eq "lua")   { $ENV{$ulib}="$c/lua";
                             $ENV{$ulib."_VER"}=$1 if($ver=~/^(\d+(\.\d+)?)/); }
    elsif($lib eq "ruby")  {
        $ENV{$ulib}="$c/ruby";
        my $eruby=globescape $ENV{$ulib};
        $ENV{$ulib."INC"}="-I".[<$eruby/include/ruby-*>]->[-1];
    }
}
sub upMove($) {
    my $target=shift;
    my $etarget=globescape $target;
    my $tdir=[<$etarget/*>]->[-1];
    return unless defined $tdir;
    my $etdir=globescape $tdir;
    print "Moving contents of $tdir to its parent directory";
    my $tmp="$target-tmp";
    my $etmp=globescape $tmp;
    mkdir $tmp
        or return 0;
    run mv => <$etdir/*> => "$target-tmp"
        or return 0;
    rmTree $tdir
        or return 0;
    run mv => <$etmp/*> => $target
        or return 0;
    rmTree $tmp;
}
sub installGNUwin32($) {
    my $project=shift;
    my $base="http://sourceforge.net/projects/gnuwin32/files/$project/";
    $mech->get($base);
    my $ver=
        [sort compareVersions
              map {s@^.*/(\d+(\.\d+)*(-\d+)?)/?$@$1@g; $_}
              map {$_->url}
                  ($mech->find_all_links(url_regex =>
                                 qr(files/\Q$project\E/\d+(\.\d+)*(-\d+)?/?$)))
        ]->[0];
    my $projurl="$base/$ver/$project-$ver-bin.zip";
    dlUncompress $projurl => "$c/$project"
        or return 0;
    addpath "$c/$project/bin";
}

do {
    print ">>> Determining library versions";

    print "::: Python";
    $mech->get("http://python.org/ftp/python");
    $libs{"python"}=
        [grep {eval {$mech->head(pyURL($_))->is_success()}}
         sort compareVersions
              map {s/[^\d.]//g; $_}
              map {$_->url}
                  ($mech->find_all_links(url_regex => qr(^2\.)))
        ]->[0];
    print "::: Python 3";
    $mech->get("http://python.org/ftp/python");
    $libs{"python3"}=
        [grep {eval {$mech->head(pyURL($_))->is_success()}}
         sort compareVersions
              map {s/[^\d.]//g; $_}
              map {$_->url}
                  ($mech->find_all_links(url_regex => qr(^3\.)))
        ]->[0];

    print "::: Perl";
    $mech->get("http://strawberryperl.com/releases.html");
    $libs{"perl"}=
        [sort compareVersions
              map {s/^.*-(\d.*)\.msi$/$1/; $_}
              map {$_->url}
                  ($mech->find_all_links(url_regex =>
                                            qr(strawberry-perl-(\d+\.)+msi$)))
        ]->[0];

    print "::: Ruby";
    $mech->get("http://rubyinstaller.org/downloads/");
    $libs{"ruby"}=
        [sort compareVersions
              map {s/^.*ruby-(\d+(?:\.\d+)*)-.*$/$1/; $_}
              map {$_->url}
                  ($mech->find_all_links(url_regex =>
                                       qr(ruby-\d+(\.\d+)*-.*\.7z(\?direct)?$)))
        ]->[0];

    print "::: Lua";
    $mech->get("http://sourceforge.net/projects/luabinaries/files");
    $libs{"lua"}=
        [sort compareVersions
              map {s@^.*/(\d+(?:\.\d+)*).*$@$1@; $_}
              map {$_->url}
                  ($mech->find_all_links(url_regex =>
                                             qr(luabinaries/files/\d+(\.\d+))))
        ]->[0];

    while(my($key, $val)=each %libs) {
        print "\t$key\:\t$val"; }
};
delete $libs{"python3"}; # Python3 causes vim to crash
my @phases=(
    wpsetup => sub {
        rmTree($wineprefix);
        run wineboot => "-i"
            or die "wineboot failed";
        for my $file (<$ewineprefix/dosdevices/*>) {
            rmTree($file) unless File::Basename::basename($file) eq "c:";
        }
        mkdir $vim
            or die "Making $vim directory failed: $!";
        1;
    },
    c32 => sub {
        my $c32="http://switch.dl.sourceforge.net/sourceforge/".
                                                       "corefonts/courie32.exe";
        my $c32target="$c/courie32.exe";
        download $c32 => $c32target
            or return 0;
        run wine => $c32target, '/Q'
            or return 0;
        unlink $c32target;
        1;
    },
    perl => sub {
        return 0 unless defined $libs{"perl"};
        $mech->get("http://strawberryperl.com/releases.html");
        my $perllink=$mech->find_link(url_regex =>
                                        qr(download/\Q$libs{"perl"}\E.*\.msi$));
        return 0 unless defined $perllink;
        installMSI($perllink->url_abs);
    },
    lua => sub {
        return 0 unless defined $libs{"lua"};
        my $luaver=$libs{"lua"};
        my $luaurlbase="http://downloads.sourceforge.net/project/luabinaries/".
                                                                  $libs{'lua'};
        my $luatailbase="lua-$luaver\_Win32_";
        my $luaurl="$luaurlbase/Executables/".$luatailbase."bin.zip";
        my $luaurl2="$luaurlbase/Windows%20Libraries/Dynamic/$luatailbase".
                                                                "dllw4_lib.zip";
        dlUncompress $luaurl2 => "$c/lua"
            or return 0;
        addpath "$c/lua";
    },
    python => sub {
        return 0 unless defined $libs{"python"};
        installPython $libs{"python"};
    },
    python3 => sub {
        return 0 unless defined $libs{"python3"};
        installPython $libs{"python3"};
    },
    tcl => sub {
        return 0 unless defined $libs{"tcl"} and defined $libs{"python"};
        (my $tclv=$libs{"tcl"})=~s/\.//g;
        (my $pyv=$libs{"python"})=~s/\.//g;
        $pyv=~s/^(..).*$/$1/;
        my $tcldll="$c/Python$pyv/DLLs/tcl$tclv.dll";
        return 0 unless -e $tcldll;
        postdll $tcldll;
    },
    ruby => sub {
        return 0 unless defined $libs{"ruby"};
        $mech->get("http://rubyinstaller.org/downloads/");
        my $rubylink=$mech->find_link(url_regex =>
                                qr(ruby-\Q$libs{"ruby"}\E.*\.7z(?:\?direct)?$));
        return 0 unless defined $rubylink;
        dlUncompress $rubylink->url_abs => "$c/ruby"
            or return 0;
        upMove "$c/ruby"
            or return 0;
        addpath "$c/ruby/bin";
    },
    vim => sub {
        map {addLib $_} keys %libs;
        run zsh => "$stufroot/build.zsh"
            or die "Failed to build vim";
        File::Copy::copy($ENV{"TMPDIR"}."/src/vim.exe",    $vim)
            or return 0;
        File::Copy::copy($ENV{"TMPDIR"}."/src/gvim.exe",   $vim)
            or return 0;
        File::Copy::copy($ENV{"TMPDIR"}."/src/vimrun.exe", $vim)
            or return 0;
        run cp => "-r", $ENV{"TMPDIR"}."/runtime", $vim;
    },
    vimregister => sub {
        run wine => "$vim/gvim.exe", "-silent", "-register";
    },
    curl => sub {
        $mech->get("http://curl.haxx.se/download.html");
        my $curllink=$mech->find_link(url_regex =>
                                qr@curl/win32/curl-\d\..*-static-bin-w32.zip$@);
        return unless defined $curllink;
        dlUncompress $curllink->url_abs => "$c/curl"
            or return 0;
        upMove "$c/curl"
            or return 0;
        addpath "$c/curl";
    },
    "7z" => sub {
        $mech->get("http://7-zip.org/download.html");
        my $szlink=$mech->find_link(url_regex => qr@/7z[^-]+.msi$@);
        return unless defined $szlink;
        installMSI($szlink->url_abs)
            or return 0;
        addpath "$c/Program Files (x86)/7-Zip";
    },
    libintl => sub { installGNUwin32("libintl");   },
    diff    => sub { installGNUwin32("diffutils"); },
    wget    => sub { installGNUwin32("wget");      },
    # patch is probably not needed as strawberry perl already has it in its 
    # distribution
    patch   => sub { installGNUwin32("patch");     },
    mercurial => sub {
        local $_=LWP::Simple::get("http://mercurial.selenic.com/sources.js");
        if(/"([^"]*\/mercurial-[^\-]+-x86.msi)"/i) {
            installMSI($1); }
        else {
            return 0; }
    },
    subversion => sub {
        $mech->get("http://www.sliksvn.com/en/download");
        my $svnlink=$mech->find_link(url_regex =>
                                          qr(slik-subversion-[^/]+-win32.msi)i);
        return unless defined $svnlink;
        installMSI($svnlink->url_abs);
    },
    # I don't know non-cygwin git and bzr package with msi or other installer 
    # that can be automated.
);
while(my ($phase, $psub)=splice @phases, 0, 2) {
    if($skipuntil) {
        if($phase eq $skipuntil) {
            undef $skipuntil; }
        else {
            print ">>> Skipping phase $phase";
            next;
        }
    }
    print ">>> Running phase $phase";
    $psub->() or print "::: Failed $phase";
    last if($stopat and $phase eq $stopat);
}
