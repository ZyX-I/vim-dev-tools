#!/bin/zsh
emulate -L zsh
setopt nullglob

: ${TMPDIR:=$HOME/tmp/build/vim-mingw32}
: ${VIMREPO:=$HOME/tmp/image/vim}
: ${ARCH:=i686}
: ${TARGET:=${ARCH}-pc-mingw32}
typeset -r TMPDIR VIMREPO ARCH TARGET

test -d $TMPDIR || mkdir -p $TMPDIR
rm -rf $TMPDIR/*(/)

path=( /usr/i686-mingw32/usr/bin $path )
typeset -xr prefix=/usr/i686-mingw32
typeset -xr vim_cv_toupper_broken=yes
typeset -xr LD=i686-pc-mingw32-ld
typeset -xr CC=i686-pc-mingw32-gcc

local -r rubyincpath=${RUBYINC[3,-1]}
test -e $rubyincpath/ruby/config.h ||
ln $rubyincpath/*/ruby/config.h $rubyincpath/ruby

local -a MAKEFLAGS
set -A MAKEFLAGS -j1 CROSS=yes ARCH=$ARCH

(
    if [[ ! -d $VIMREPO ]] ; then
        test -d $VIMREPO:h || mkdir -p $VIMREPO:h
        git clone https://github.com/vim/vim $VIMREPO
    else
        (cd $VIMREPO && git pull)
    fi
    git clone --depth=1 $VIMREPO $TMPDIR
    MAKEFLAGS+=( FEATURES=HUGE CROSS_COMPILE=${TARGET}- OPTIMIZE=SPEED
                 VIMRUNTIMEDIR='C:\vim\runtime'
                 ${(f)"$(typeset -m {PYTHON,PERL,LUA,RUBY}\*)"}
               )
    cd $TMPDIR/src                                                &&
    make -f Make_ming.mak $MAKEFLAGS GUI=yes NETBEANS=no gvim.exe &&
    make -f Make_ming.mak $MAKEFLAGS GUI=no               vim.exe &&
    make -f Make_ming.mak $MAKEFLAGS GUI=no            vimrun.exe &&
    true
)
