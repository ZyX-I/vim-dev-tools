#compdef test_zsh
emulate -L zsh
local -r STUFDIR=~/.vim/dev/repository
local -r TESTSCRIPT=$STUFDIR/test.zsh
local    DEVDIR=$STUFDIR:h
local -i FOREGROUND STOPJOBS
local -a ADDARGS RESTARGS
local    TESTREV TESTTYPE VIMPATH CT ADDON VIRTENV
function _test_zsh_cmd()
{
    $@ 2>/dev/null
}
function _test_zsh_getenv()
{
    eval "$($TESTSCRIPT -= ${words[2,-1]} 2>/dev/null)"
}
function _test_zsh_revisions()
{
    _test_zsh_getenv
    typeset -a revisions
    local tag rev
    local -a HGOPTS
    HGOPTS=( -R $DEVDIR/$ADDON )
    cat <(_test_zsh_cmd hg $HGOPTS tags     ) \
        <(_test_zsh_cmd hg $HGOPTS branches ) \
        <(_test_zsh_cmd hg $HGOPTS bookmarks) |
    while read tag rev ; do
        revisions+=( ${tag//:/\\:}:${rev} )
    done
    (( $#revisions )) && _describe -t revisions 'revisions' revisions
}
function _test_zsh_plugs()
{
    [[ -e ${REPLY}/${REPLY:t}-addon-info.txt ]]
}
function _test_zsh_lsaddons()
{
    _test_zsh_getenv
    local -a ADDONS
    ADDONS=( $DEVDIR/*(/+_test_zsh_plugs) )
    compadd -- $ADDONS:t
}
function _test_zsh_lstests()
{
    _test_zsh_getenv
    setopt nullglob extendedglob
    local -a TESTS
    if [[ $TESTREV == . ]] ; then
        TESTS=( $DEVDIR/$ADDON/test/*.(in|ok) )
    else
        TESTS=( ${(0)"$(_test_zsh_cmd hg -R $DEVDIR/$ADDON locate \
                                -r $TESTREV -0 test/'*.{in,ok}')"} )
    fi
    compadd -- $TESTS:t:r
}
function _test_zsh_venvs()
{
    if [[ -z $(whence -w _virtualenvs) ]] ; then
        if ! [[ -z $(whence -w virtualenvwrapper_setup_tab_completion) ]] ; then
            virtualenvwrapper_setup_tab_completion
        else
            exit 1
        fi
    fi
    if [[ -z $(whence -w _virtualenvs) ]] ; then
        exit 1
    fi
    local -a reply
    _virtualenvs
    compadd -- $reply
}
_arguments -S \
    -h'[Show help]' \
    -b'[Run tests in background]' \
    -f'[Run tests in foreground]' \
    -s'[Don’t execute new tests after one of the tests failed]' \
    -r'[Get revision]:revision:_test_zsh_revisions' \
    -t'[Type of the test]:test types:( vim gvim wine gwine )' \
    -p'[Path to vim executable]:directories:_path_files -/' \
    -a'[Tested vim plugin]:addons:_test_zsh_lsaddons' \
    -T'[Launch only this test]:tests:_test_zsh_lstests' \
    -d'[Directory with addons]:directories:_path_files -/' \
    -v'[Virtual environment to work with]:virtual environments:_test_zsh_venvs'
