#!/bin/zsh
emulate -LR zsh
typeset -gr DEVDIR=$0:a:h:h
. ${0:a:h}/parjobs.zsh
# MAXJOBS=1
setopt extendedglob
setopt nullglob
for d in $DEVDIR/*/doc(/) ; do
    addjob \
    vim -u NONE -c "execute 'helptags '.fnameescape(${(qqq)d})"
done
waitjobs
