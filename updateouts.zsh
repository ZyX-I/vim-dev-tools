#!/bin/zsh

emulate -L zsh

autoload colors && colors
autoload zmv

setopt nullglob

typeset -gr STUFDIR=$0:a:h

typeset -g ADDONDIR=$PWD
while ! test -e $ADDONDIR/.hg ; do
    ADDONDIR=${ADDONDIR:h}
done
typeset -gr ADDON=${ADDONDIR:t}
test -e $ADDONDIR/${ADDON}-addon-info.txt || exit 1
typeset -gr TESTDIR=$ADDONDIR/test
typeset -gr PREFIX=$TESTDIR/$PREFIX

typeset -xr TMPDIR=${TMPBASE:-/tmp}/${ADDON}-tests-${TNUM:-0}/t

integer -g  YES=0
if [[ $1 == -y ]] ; then
    YES=1
    shift
fi
integer -gr YES

function copyout()
{
    othername=$TMPDIR/test/$1
    cp $othername.out $PREFIX/$1.ok
    if [[ -e "$TESTDIR/copyout-postproc.zsh" ]] ; then
        . $TESTDIR/copyout-postproc.zsh $PREFIX/$1.ok
    fi
}

test -d $PREFIX || mkdir $PREFIX

if test -e $TMPDIR/failed.lst ; then
    for l in $(cat $TMPDIR/failed.lst | cut -d' ' -f 2) ; do
        othername=$TMPDIR/test/$l
        if (( !YES )) ; then
            vimdiff -u NONE -N -c 'nnoremap ,n :cq<CR>' \
                               -c 'nnoremap ,y :qa!<CR>' \
                $othername.ok $othername.out || continue
        fi
        copyout $l
    done
fi

for TEST in $TMPDIR/test/*.in ; do
    TEST=$TEST:t:r
    othername=$TMPDIR/test/$TEST
    test -e $othername.ok  && continue
    test -e $othername.out || continue
    echo
    if ! (( YES )) ; then
        cat $othername.out
        echo -n "\n\e[${color[yellow]}mCopy $TEST?\e[0m "
        read -q copy || continue
    fi
    copyout $TEST
done
