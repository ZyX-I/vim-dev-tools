#!/bin/zsh

typeset -A has
typeset -A tests

cat musthave.dat |
while read test spec ; do
    tests[$test]="$spec"
    for req in ${(s:,:)spec} ; do
        for alt in ${(s:|:)req} ; do
            has[$alt]=-1
        done
    done
done

(
function lvim()
{
    local -x FOREGROUND=1
    SESNAME=vim-has \
    vimcmd -u NONE \
        --cmd 'set guifont=Courier_New:h10:cRUSSIAN' \
        -c "call writefile(map(['${(kj.', '.)has}'], 'v:val.'' ''.has(v:val)'), 'has', 'b')" \
        -c q
}
lvim
)

has=( $(cat has) )

for test in ${(k)tests} ; do
    spec=${tests[$test]}
    for req in ${(s:,:)spec} ; do
        integer reqmet=0
        for alt in ${(s:|:)req} ; do
            if (( ${has[$alt]} )) ; then
                reqmet=1
                break
            fi
        done
        if ! (( reqmet )) ; then
            rm -f $test.in
            break
        fi
    done
done

