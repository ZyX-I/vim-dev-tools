#!/bin/zsh
emulate -L zsh
typeset -gr DEVDIR=$0:a:h:h
typeset -gr STUFDIR=$0:a:h
typeset -gr DOCDB=~/.db/vim-help-times.dat
. $STUFDIR/gettemp.zsh format-help
typeset -gr DOCDIR=$TMPDIR/doc
typeset -gr DOCREPODIR=$STUFDIR/vimcommunity.bitbucket.org
typeset -gr DOCTGTDIR=$DOCREPODIR/doc
setopt extendedglob
setopt nullglob
setopt rcquotes

typeset -gr P=$0:t
function help()
{
local s=${P//?/ }
cat <<EOF
    $0 --help
    $0 [--nopost] [--foreground] [--all] [{filename}*]
EOF
exit 0
}

[[ $1 == --help ]] && help
if [[ $1 == --nopost ]] ; then
    typeset -gir NOPOST=1
    shift
else
    typeset -gir NOPOST=0
fi
if [[ $1 == --foreground ]] ; then
    typeset -gr EXITARG
    typeset -gar ADDJOB
    shift
else
    EXITARG="|qa!"
    ADDJOB=( addjob )
    typeset -gr EXITARG
    typeset -gar ADDJOB
fi
if [[ $1 == --all ]] ; then
    typeset -gir ALL=1
    shift
else
    typeset -gir ALL=0
fi

function plugtxt()
{
    if [[ -d ${REPLY}/doc ]] ; then
        reply=( ${REPLY}/doc/*.(txt|??x) )
    else
        return 1
    fi
}

function plugs()
{
    [[ -e ${REPLY}/${REPLY:t}-addon-info.txt ]]
}

function format()
{
    local helpfile
    local -x COLRTP=$HOME/.vam/vimcolorschemetest-colors
    for helpfile in $@ ; do
        local htmlfile=$helpfile.html
        if [[ $helpfile[-1] == 'x' ]] ; then
            local lang=$helpfile[-3,-2]
            local tags=./tags-$lang,./tags
        else
            local lang=en
            local tags=./tags
        fi
        $ADDJOB \
        vim -g -f \
          -R -u NONE --cmd 'set nocompatible t_Co=256 bg=light' \
             -i NONE --cmd 'let &rtp.=",".$RTPS.",".$COLRTP' \
                     --cmd 'syntax on' \
                     -c "set ft=help cole=2 nolist hlg=$lang tag=$tags" \
                     -c 'let g:format_HTMLTitleExpr=''":h ".expand("%:t")''' \
                     -c 'let g:format_IgnoreTags=0' \
                     -c 'let g:format_FormatConcealed=1' \
                     -c 'let g:format_FormatMatches="none"' \
                     -c 'let g:format_HTMLUseTagNameInAnchor=1' \
                     -c 'let g:format_StartTagReg=''|\|\%(''''\l\)\@=''' \
                     -c 'let g:format_EndTagReg=''\%(\l''''\)\@<=\*\@!\||''' \
                     -c 'runtime! plugin/format.vim' \
                     -c "Format format html to ${htmlfile}${EXITARG}" \
                     $helpfile
    done
    waitjobs
}

. $STUFDIR/parjobs.zsh
function printjob()
{
    echo -n ${${${(P)1}[-1]}:t}
}

local -A helpfiletimes
if (( ! ALL )) && test -r $DOCDB ; then
    helpfiletimes=( $(< $DOCDB) )
fi

mkdir -p $DOCDIR
local -a helpfiles
local -a allhelpfiles
hg -R ~/a.a/Proj/c/vim-upstream pull -u
for helpfile in $DEVDIR/*(/+plugtxt) \
    ~/a.a/Proj/c/vim-upstream/runtime/doc/*.(txt|??x)(^@)
do
    local hf=$helpfile:t
    local -i modtime=$(stat -c %Y $helpfile)
    allhelpfiles+=( $helpfile )
    if (( ${+helpfiletimes[$hf]} )) && (( ${helpfiletimes[$hf]}>=$modtime ))
    then
        continue
    else
        helpfiles+=( $hf )
        helpfiletimes[$hf]=$modtime
    fi
done
if (( !$#helpfiles && !$# )) ; then
    echo 'No new help files found' >&2
    exit 1
fi
for helpfile in $allhelpfiles ; do
    cp $helpfile $DOCDIR
done
vim -u NONE -c "execute 'helptags '.fnameescape(${(qqq)DOCDIR})" -c 'qa!'
cd $DOCDIR
set -A RTP ${DEVDIR}/*(/+plugs)
export RTPS=${(j.,.)RTP}
export DOCDIR
if (( $# )) ; then
    format $DOCDIR/${^@}
else
    format $DOCDIR/$^helpfiles
fi
cp --target-directory=$DOCTGTDIR $DOCDIR/*.html
for f in $DOCDIR/*.html ; do
    hg -R $DOCREPODIR add $DOCTGTDIR/$f:t
done
hg -R $DOCREPODIR commit -m 'Updated formatted help files'
hg push -R $DOCREPODIR && echo ${(kv)helpfiletimes} > $DOCDB
