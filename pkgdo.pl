#!/usr/bin/perl
#▶1 use
use strict;
use warnings;

use YAML::XS;
use File::Basename;
use Cwd;
use WWW::Mechanize;
use HTML::Entities;
use File::Glob;
use File::Path;
use File::Copy;
use File::Temp;
use URI::URL;
use URI::Escape;
use Encode;
use HTTP::Cookies;
use Term::ReadKey;
use utf8;
#▶1 Объявление глобальных переменных
#▶2 Аргументы командной строки
my $action = shift @ARGV;
my $addon  = shift @ARGV;
if($addon eq '.') {
    $addon = File::Basename::basename(Cwd::realpath('.'));
}
#▶2 Каталоги и файлы
my $stufroot = $ENV{"STUFDIR"} || File::Basename::dirname(Cwd::realpath($0));
my $devroot  = $ENV{"DEVDIR"}  || File::Basename::dirname($stufroot);
my $reporoot = "$stufroot/pluginloader-overlay";

my $distdir  = "$stufroot/distfiles";
my $tmpdir   = $ENV{"TMPDIR"} || "/tmp/pkgdo";

my ($addroot, $addinfo, $defhelpfile, $docbasename);
if(defined $addon) {
    $addroot     = "$devroot/$addon";
    $addinfo     = "$addroot/$addon-addon-info.txt";
    $defhelpfile = "$addroot/doc/$addon.txt";
    unless (-e $addinfo) {
        $addinfo = "$addroot/addon-info.json";
    }
    unless (-e $defhelpfile) {
        $defhelpfile = "$1.txt" if($defhelpfile =~ /^(.*)vim\.txt$/);
    }
    $docbasename = $1 if($defhelpfile =~ /\/([^\/]+)\.txt$/);
}
my $passfile=$ENV{"HOME"}.'/.settings/passwords.yaml';
my $passwords=YAML::XS::LoadFile($passfile)
    if -r $passfile;

$ENV{"DISTDIR"}=$distdir;
#▲2
# FIXME put this into dependency to help file name
my $docbase="http://vimcommunity.bitbucket.org/doc/$docbasename.txt.html";
my $cookie_jar = HTTP::Cookies->new(
    file => "$ENV{'HOME'}/.cache/pkdo_cookies.dat",
    autosave => 1,
);
my $mech=WWW::Mechanize->new(autocheck => 1, cookie_jar => $cookie_jar);
my $loggedin=0;
binmode(*STDOUT, ":utf8");
#▶1 Функции
#▶2 lsdir :: dir -> [filename]
sub lsdir($;$) {
    my $dir=shift;
    my $prependdir=shift;
    my $DH;
    opendir $DH, $dir or die "Failed to open directory $dir";
    local $_;
    my $f;
    if($prependdir) {
        $f=sub {"$dir/".(shift)}; }
    else {
        $f=sub {shift}; }
    my @r=map {$f->($_)} (grep {$_!~/^\./} readdir($DH));
    closedir $DH;
    return @r;
}
#▶2 getAddonInfo :: addon -> addhash
sub getAddonInfo($) {
    my $addon   = shift;
    return $addon if(ref $addon);
    my $addinfo  = "$devroot/$addon/$addon-addon-info.txt";
    unless (-e $addinfo) {
        $addinfo = "$devroot/$addon/addon-info.json";
    }
    -e $addinfo
        or die "No addon info file found";

    return YAML::XS::LoadFile($addinfo);
}
#▶2 listDependencies :: addon -> [addname]
sub listDependencies($) {
    my $addon    = shift;
    my $infohash = getAddonInfo($addon);
    return keys %{$infohash->{"dependencies"}};
}
#▶2 listAllDependencies :: addon -> [addname]
sub listAllDependencies($) {
    my $addon=shift;
    my @toprocess=listDependencies($addon);
    $addon=$addon->{"name"} if(ref $addon);
    my %dependencies=();
    while(@toprocess) {
        my $a = shift @toprocess;
        unless(defined($dependencies{$a})) {
            $dependencies{$a}=1;
            push @toprocess, listDependencies($a);
        }
    }
    delete $dependencies{$addon}
        if(defined $dependencies{$addon});
    return keys %dependencies;
}
#▶2 getRepoLocation :: addon -> url
sub getRepoLocation($) {
    my $addon    = shift;
    my $infohash = getAddonInfo($addon);
    $addon=$addon->{"name"} if(ref $addon);
    return $infohash->{"repository"}{"url"}
        if(defined $infohash->{"repository"}{"url"});
    my $addroot  = "$devroot/$addon";
    my $addhgini = "$addroot/.hg/hgrc";
    my $HGINI;
    open $HGINI, "<", $addhgini
        or die "Local hgrc file not found";
    foreach (<$HGINI>) {
        if(s/^default\s*=\s*//) {
            s/\s+$//;
            close $HGINI;
            return $_;
        }
    }
}
#▶2 updateField :: addon, field:s, value:s -> _
sub updateField($$$) {
    my $addon   = shift;
    $addon=$addon->{"name"} if(ref $addon);
    my $field   = shift;
    my $value   = shift;
    my $FN;
    my $text="";
    my $addinfo  = "$devroot/$addon/$addon-addon-info.txt";
    unless (-e $addinfo) {
        $addinfo = "$devroot/$addon/addon-info.json";
    }
    open $FN, '<', $addinfo
        or die "Opening addon info file failed";
    my $substituted=0;
    foreach (<$FN>) {
        if(not $substituted and s/(^ {4}"$field":\s*)"?.*?"?(?=,)/$1"$value"/) {
            $substituted=1;
        }
        if(not $substituted and /^}/) {
            $text.="    \"$field\": \"$value\",\n";
        }
        $text.=$_;
    }
    close $FN;
    open $FN, '>', $addinfo
        or die "Opening addon info file for writing failed";
    print $FN $text;
    close $FN;
}
#▶2 login :: () -> _ + $mech
sub login() {
    return if($loggedin);
    my ($user, $password)=%{$passwords->{"www.vim.org"}[0]};
    $mech->get("http://www.vim.org/login.php");
    $mech->submit_form(
        form_name => "login",
        with_fields => {
            userName => $user,
            password => $password,
        },
    );
    die $mech->content()
        if $mech->content() =~ /POST has been used/;
    $cookie_jar->save();
    $loggedin=1;
}
#▶2 goToScript :: addon -> _ + $mech
sub goToScript($) {
    my $addon=shift;
    my $infohash=getAddonInfo($addon);
    my $scriptnr=$infohash->{"scriptnumber"};
    unless($scriptnr) {
        warn "Unable to post script $addon\: unknown script number";
        return;
    }
    login();
    $mech->get("http://www.vim.org/scripts/script.php?script_id=$scriptnr");
}
#▶2 postScript :: addon, file, versionComment[, vimVersion=7.3] -> _
sub postScript($$$;$) {
    my $addon  = shift;
    my $file   = shift;
    my $vercom = shift;
    my $vimver = shift || "7.3";
    my $infohash=getAddonInfo($addon);
    $addon=$addon->{"name"} if(ref $addon);
    $vercom=Encode::decode_utf8($vercom)
        unless Encode::is_utf8($vercom);
    $vercom=HTML::Entities::encode_entities($vercom);
    goToScript($infohash) or return;
    $mech->follow_link(text => 'upload new version');
    $mech->form_name("script");
    $mech->field(script_file => $file);
    $mech->field(vim_version => $vimver);
    $mech->field(script_version => $infohash->{"version"});
    $mech->field(version_comment => $vercom);
    $mech->click_button(value => "upload");
}
#▶2 getProjectPage :: addon → URL
sub getProjectPage($) {
    my $addon=shift;
    my $infohash=getAddonInfo($addon);
    $addon=$addon->{"name"} if(ref $addon);
    my $repo=getRepoLocation($infohash);
    if($repo =~ /bitbucket\.org\/(.*)/) {
        return "https://bitbucket.org/$1";
    }
    elsif($repo =~ /(\w+)\.hg\.(?:sourceforge|sf)\.net/) {
        return "http://sf.net/projects/$1";
    }
}
##▶2 refield :: fieldName → + field
sub refield($) {
    my $field=shift;
    my $value=$mech->value($field);
    $value=Encode::decode('CP1252', $value) unless Encode::is_utf8($value);
    $mech->field($field => HTML::Entities::encode_entities($value));
}
#▶2 wpupdfuncs
my %wpupdfuncs=(
    moinmoin => sub {
        my $addon  = shift;
        my $url    = shift;
        my $readme = shift;
        my $chcom  = shift;
        my $htags  = shift;
        $url=URI::URL->new($url) unless ref $url;
        my ($user, $password)=findPass($url);
        $mech->get("$url?action=login");
        $mech->submit_form(
            with_fields => {
                name     => $user,
                password => $password,
            },
        );
        $mech->get("$url?action=edit&editor=text");
        my $fieldname="savetext";
        $mech->form_with_fields($fieldname);
        my @text=split /(?<=\n)/, $mech->current_form->value($fieldname);
        local $_;
        my ($offset, $length);
        my $i=0;
        for (@text) {
            if($_ eq "== Overview ==\n") {
                $offset=$i+1; }
            elsif(defined $offset and /^==/) {
                $length=$i-$offset;
                last;
            }
        }
        continue {$i++;}
        my @rlines;
        if(ref $readme) {
            @rlines=map {
                    local $_=$_;
                    $_=Encode::decode_utf8($_) unless Encode::is_utf8($_);
                    $_.="\n" unless(/( |^)$/);
                    s/ \n/ /;
                    s@([\w\-]+) \((https?://[^)]+)\)@[[$2|$1]]@g;
                    s@([\w\-]+) \(vimscript #(\d+)\)@[[http://www.vim.org/scripts/script.php?script_id=$2|$1]]@g;
                    s/\|([^| ]+)\|/defined $htags->{$1} ?
                                    ("[[$docbase#".URI::Escape::uri_escape($1).
                                     "|$1]]"):
                                    "$1"/ge;
                    s/^( *)- /$1* /;
                    $_;
                } @$readme;
        }
        else {
            @rlines=($readme);
        }
        push @rlines, "\n";
        splice @text, $offset, $length, @rlines;
        $mech->field($fieldname => (join "", @text));
        $mech->field(comment    => $chcom);
        $mech->click_button(name => "button_save");
    },
);
#▶2 anchorEscape :: char → s
sub anchorEscape($) {
    my $char = shift;
    my $num = ord $char;
    my $r = '';
    while($num) {
        $r .= sprintf ".%02x", $num&0xFF;
        $num = $num>>16;
    }
    return $r;
}
#▶2 GetVOTagged :: range, args + ? → [string]
sub GetVOTagged($@) {
    my $range = shift;
    my $t=File::Temp->newdir();
    $ENV{"t"}=$t->dirname."/file";
    my $DUMMY;
    open $DUMMY, '-|',
           vim => "-c", 'set cole=2 nolist helplang=',
                  "-c", 'let g:format_StartTagReg=\'|\|\%(\'\'\l\)\@=\'',
                  "-c", 'let g:format_EndTagReg=\'\%(\l\'\'\)\@<=\*\@!\||\'',
                  "-c", 'let g:format_IgnoreTags=0',
                  "-c", 'let g:format_FormatConcealed=1',
                  @_,
                  "-c", $range.'Format f vimorg-tagged to $t',
                  "-c", 'qa!';
    1 while (<$DUMMY>);
    my $T;
    open $T, '<:utf8', $ENV{"t"}
        or die "Failed to open $ENV{'t'}";
    my $r=[<$T>];
    undef $t;
    return $r;
}
#▶2 ReformatAsVOTagged :: string + tags → string
sub ReformatAsVOTagged($) {
    my $string = shift;
    $ENV{"s"} = $string;
    my $rlines = GetVOTagged('%',
        "-c", 'set buftype=help filetype=help',
        "-c", 'call setline(".", split($s, "\n"))',
    );
    return join "", @$rlines;
}
#▶2 formatScriptReadme :: addon, readme, helptags, encodeEntities → readme
sub formatScriptReadme($$$$) {
    my $addon          = shift;
    my $readme         = shift;
    my $htags          = shift;
    my $encodeEntities = shift;
    my $infohash=getAddonInfo($addon);
    $addon=$addon->{"name"} if(ref $addon);
    local $_;
    my $lasttagn=0;
    my $helprlines=GetVOTagged('/^1\. Intro/+1;/^==/-1',
        "-c", ":h ".File::Basename::basename($defhelpfile),
    );
    my @rlines=();
    my $pushed=0;
    my @additional_lines = (
        "Project page: ".getProjectPage($infohash)."\n",
        "Documentation: $docbase\n",
    );
    if(-r "$addroot/doc/$addon.rux") {
        (my $rudocbase=$docbase)=~s/\.txt\.html$/\.rux\.html/;
        push @additional_lines, "Russian documentation: $rudocbase\n";
    }
    push @additional_lines, "\n\n";
    foreach (@$helprlines) {
        if(!$pushed and /^\[\d+\] /) {
            push @rlines, @additional_lines;
            $pushed=1;
        }
        push @rlines, $_;
    }
    if(!$pushed) {
        push @rlines, @additional_lines;
    }
    push @rlines, "\n";
    my $r=join "", @rlines;
    $r = HTML::Entities::encode_entities($r) if($encodeEntities);
    return $r;
}
#▶2 updateScriptPage :: addon, summary, readme, installDetails, helptags → + net
sub updateScriptPage($$$$$) {
    my $addon    = shift;
    my $summary  = shift;
    my $readme   = shift;
    my $idetails = shift;
    my $htags    = shift;
    my $infohash=getAddonInfo($addon);
    $addon=$addon->{"name"} if(ref $addon);
    goToScript($infohash) or return;
    $mech->follow_link(text => 'edit details');
    $mech->form_name("script");
    if(ref $readme) {
        my $formattedreadme = formatScriptReadme($infohash, $readme, $htags, 1);
        $mech->field(description => $formattedreadme);
    }
    elsif($readme) {
        $readme=Encode::decode_utf8($readme) unless Encode::is_utf8($readme);
        $mech->field(description => HTML::Entities::encode_entities($readme));
    }
    else {
        refield("description"); }
    if($summary) {
        $mech->field(summary => $summary); }
    else {
        refield("summary"); }
    $idetails=Encode::decode_utf8($idetails) unless Encode::is_utf8($idetails);
    if($idetails) {
        $mech->field(install_details =>
                                HTML::Entities::encode_entities($idetails)); }
    else {
        refield("install_details"); }
    $mech->click_button(value => "update");
}
#▶2 updateWikis :: addon, summary, readme, chcom, helptags → + net
sub updateWikis {
    my $addon    = shift;
    my $summary  = shift;
    my $readme   = shift;
    my $chcom    = shift;
    my $helptags = shift;
    my $infohash=getAddonInfo($addon);
    $addon=$addon->{"name"} if(ref $addon);
    if(defined $infohash->{"wiki_pages"}) {
        my $wkurls=$infohash->{"wiki_pages"};
        while(my ($wname, $wfunc)=each %wpupdfuncs) {
            if(defined $wkurls->{$wname}) {
                local $_;
                map {$wfunc->($addon, $_, $readme, $chcom, $helptags)}
                    @{$wkurls->{$wname}};
            }
        }
    }
}
#▶2 findPass
sub findPass($) {
    my $url=shift;
    $url=URI::URL->new($url) unless ref $url;
    (my $host=$url->ihost())=~s/^www\.//;
    my @path=$url->path_segments();
    $path[0]=$host;
    my ($user, $password);
    local $_;
    my $curpath="";
    for (@path) {
        $curpath=(($curpath)?("$curpath/"):("")).$_;
        if(defined $passwords->{$curpath}) {
            ($user, $password)=%{$passwords->{$curpath}[0]};
        }
    }
    die "Failed to find login for $url" unless defined $user;
    return ($user, $password);
}
#▶2 removeScript :: addon[, regex] -> _
sub removeScript($;$) {
    my $addon=shift;
    my $regex=shift;
    my $infohash=getAddonInfo($addon);
    goToScript($infohash) or return;
    if($regex) {
        my $link=$mech->find_link(text_regex => qr($regex),
                                   url_regex => qr(download_script));
        unless(defined $link) {
            warn "Failed to find version $regex";
            return;
        }
        (my $srcid=$link->url)=~s/^.*src_id=(\d+).*$/$1/;
        $mech->follow_link(url_regex => qr(del_script_version.*$srcid));
    }
    else {
        $mech->follow_link(url_regex => qr(del_script_version)); }
    $mech->form_with_fields(qw(script_source_id));
    $mech->click_button(value => "yep");
}
#▶2 bblogin
sub bblogin() {
    my ($user, $password)=%{$passwords->{"bitbucket.org"}[0]};
    $mech->get("https://bitbucket.org/account/signin/");
    $mech->submit_form(
        with_fields => {
            username => $user,
            password => $password,
        },
    );
    return $user;
}
#▶2 updateRepoSummary :: addon, summary → + net
sub updateRepoSummary($$) {
    my $addon   = shift;
    my $summary = shift;
    my $infohash=getAddonInfo($addon);
    $addon=$addon->{"name"} if(ref $addon);
    my $repo=getRepoLocation($infohash);
    if($repo=~/bitbucket/) {
        my $user=bblogin();
        $mech->get("https://bitbucket.org/$user/$addon/admin");
        $mech->submit_form(with_fields => {description => $summary});
    }
}
#▶2 postArch :: addon, file -> _
sub postArch($$) {
    my $addon=shift;
    my $arch=shift;
    my $infohash=getAddonInfo($addon);
    $addon=$addon->{"name"} if(ref $addon);
    my $repo=getRepoLocation($infohash);
    if($repo=~/sourceforge/) {
        system scp => $arch,
            "zyxsf,$addon\@frs.sourceforge.net:".
                "/home/frs/project/".substr($addon, 0, 1).
                                 "/".substr($addon, 0, 2)."/$addon";
    }
    elsif($repo=~/bitbucket/) {
        my $user=bblogin();
        my $fname=File::Basename::basename($arch);
        $mech->get("https://bitbucket.org/$user/$addon/downloads");
        $mech->form_with_fields(qw(file));
        $mech->field(file => $arch);
        $mech->field(key  => $mech->value("key").$fname);
        $mech->submit();
    }
}
#▶2 prepareEbuild :: addon[, version] -> ebuild
sub prepareEbuild($;$) {
    my $addon   = shift;
    my $version = shift || "";
    my $infohash=getAddonInfo($addon);
    my $body=<<"EOF";
# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# \$Header: \$
EAPI=3
inherit vim-plugin
EOF
    if($version eq "9999") {
        $body.="inherit mercurial\n".
                 "EHG_REPO_URI=\"".getRepoLocation($addon)."\"\n".
                 "SRC_URI=\"\"\n".
                 "S=\"\${WORKDIR}/\${PN}\"\n";
    }
    else {
        my $repo=getRepoLocation($infohash);
        my $src_uri;
        if($repo=~/sourceforge/) {
            $src_uri='http://downloads.sourceforge.net/project/$PN/$P.tar.xz';
        }
        elsif($repo=~/bitbucket/) {
            my ($user, $password)=%{$passwords->{"bitbucket.org"}[0]};
            $src_uri="http://bitbucket.org/$user/\$PN/downloads/\$P.tar.xz";
        }
        $body.="KEYWORDS=\"~alpha ~amd64 ~ia64 ~ppc ~ppc64 ~sparc ~x86 ".
                          "~amd64-linux ~x86-linux ~ppc-macos ~x86-macos ".
                          "~sparc-solaris\"\n".
               "SRC_URI=\"$src_uri\"\n";
    }
    my @dependencies=listDependencies($infohash);
    my $depstring="RDEPEND=\"";
    if(@dependencies) {
        $depstring.=(join(("\n".(" " x 9)), map {"app-vim/$_"} @dependencies));
    }
    my $otherdeps=$infohash->{"RDEPEND"};
    if(defined $otherdeps) {
        local $_;
        foreach (@$otherdeps) {
            $depstring.="\n".(" " x 9).$_;
        }
    }
    my $useflags=$infohash->{"USE"};
    if(defined $useflags) {
        $body.="USE=\"".(join(" ", keys(%$useflags)))."\"\n";
        for my $key (keys %$useflags) {
            $depstring.="\n".(" " x 9)."$key? ( $useflags->{$key} )"
        }
        $body.="IUSE=\"\$USE\"";
    }
    $depstring.="\"\n";
    $body.=$depstring;
    my $description=$infohash->{"description"};
    if($description) {
        $body.="DESCRIPTION=\"$description\"\n";
    }
    $body.=<<"EOF";

src_prepare() {
	rm -rf .hg* test samples
}
EOF
    return $body;
}
#▶2 makeEbuild :: addon[, version=current] -> _
sub makeEbuild($;$) {
    my $addon = shift;
    my $infohash=getAddonInfo($addon);
    $addon=$addon->{"name"} if(ref $addon);
    my $addroot = "$devroot/$addon";
    my $version = shift || $infohash->{"version"};
    if($version ne "9999") {
        my $arch="$distdir/$addon-$version.tar";
        TarArchive($addroot, "release-$version", $arch);
        system xz => "-f", $arch;
        $arch="$arch.xz";
        postArch($infohash, $arch);
    }
    my $edir="$reporoot/app-vim/$addon";
    mkdir $edir
        or die "Failed to create directory $edir"
            unless -d $edir;
    my $efile="$edir/$addon-$version.ebuild";
    my $FN;
    open $FN, '>', $efile
        or die "Failed to open $efile for writing";
    print $FN prepareEbuild($infohash, $version);
    close $FN;
    system ebuild => $efile, "manifest";
    AddFile($reporoot, $efile);
    Commit($reporoot, "Added/updated $addon $version ebuild");
}
#▶2 compareVersions :: version, version -> -1|0|1
sub compareVersions($$) {
    my $v=shift;
    my $ov=shift;
    my @p=split /\./, $v;
    my @op=split /\./, $ov;
    while(@p) {
        my $comp=(((shift@p)||0)<=>((shift@op)||0));
        if($comp != 0) {
            return $comp;
        }
    }
}
#▶2 setNewVersion :: version|version_increment, version -> version
sub setNewVersion($$) {
    local $_=shift;
    my $ov=shift;
    if(/^\++$/) {
        my @vcomp=(split /\./, $ov);
        $#vcomp=(length)-1;
        $vcomp[-1]++;
        return join('.', @vcomp);
    }
    elsif($_ eq '.') {
        return $ov;
    }
    elsif(/^\d+(\.\d+)+/) {
        return $_;
    }
    die "Invalid version: $_";
}
#▶2 listHelpFiles :: addon -> [file]
sub listHelpFiles($) {
    my $addon   = shift;
    my $docroot = "$devroot/$addon/doc";
    return () unless -d $docroot;
    return(File::Glob::bsd_glob("$docroot/*.{txt,??x}"));
}
#▶2 fnameescape :: file -> String
sub fnameescape($) {
    local $_=shift;
    s/(["\\])/\\$1/g;
    s/\n/\\n/g;
    return "\"$_\"";
}
#▶2 listPlugins
sub listPlugins() {
    my $DIR;
    opendir $DIR, $devroot
        or die "Failed to open directory $devroot";
    my @r=();
    local $_;
    foreach (readdir $DIR) {
        push @r, $_ if(-e "$devroot/$_/$_-addon-info.txt" || -e "$devroot/$_/addon-info.json"); }
    closedir $DIR;
    return @r;
}
#▶2 makeArchive
sub makeArchive($;$) {
    my $addon   = shift;
    $addon=$addon->{"name"} if(ref $addon);
    my $infohash=getAddonInfo($addon);
    my $version = shift || $infohash->{"version"};
    my $arch="$stufroot/voarchives/$addon-$version.tar.gz";
    unlink $arch if(-e $arch);
    makeBundleDirectory($infohash);
    system tar => "-czvf", $arch, "-C", $tmpdir, ".",
                  "--owner", "root", "--group", "root";
    return $arch;
}
#▶2 makeBundleDirectory
sub makeBundleDirectory($;$$$) {
    my $addon     = shift;
    my $targetdir = shift || $tmpdir;
    my $skipaddon = shift;
    my $uselatest = shift;
    my $infohash  = getAddonInfo($addon);
    $addon=$addon->{"name"} if(ref $addon);
    for my $dep (listAllDependencies($infohash), $skipaddon ? () : $addon) {
        my $deproot = "$devroot/$dep";
        my $dephash = getAddonInfo($dep);
        my $depver  = $dephash->{"version"};
        my $deprev  = ($uselatest or $depver eq "0.0") ?
                            "tip" :
                            "release-$depver";
        CopyFiles($deproot, $deprev, $targetdir);
        cleanTMP('{.hg*,test,*-addon-info.txt,addon-info.json,samples}', $targetdir);
    }
}
#▶2 listVersions
sub listVersions($) {
    my $addon=shift;
    my $infohash=getAddonInfo($addon);
    $addon=$addon->{"name"} if(ref $addon);
    my $addroot = "$devroot/$addon";
    return
        sort {compareVersions($a, $b)}
            map {s/^release-//;$_}
                grep /^release-/ => ListTags($addroot);
}
#▶2 cleanTMP
sub cleanTMP(;$$) {
    my $pat  = shift;
    local $_ = shift || $tmpdir;
    unless(-d $_) {
        File::Path::mkpath($_);
        return;
    }
    unless(defined $pat) {
        File::Path::remove_tree(lsdir("$_"));
    }
    else {
        s/(\\\[\]\{\}\*\?~)/\\$1/g;
        File::Path::remove_tree(File::Glob::bsd_glob("$_/$pat"));
    }
}
#▶2 Templates
my %templates=(
    doc => sub {
        my $addon    = shift;
        my $fname    = "doc/$addon.txt";
        my $infohash = getAddonInfo($addon);
        my $ruler    = "=" x 78;
        my $ruler2   = "-" x 78;
        my $r="*$addon.txt* ".$infohash->{'description'}."\n$ruler\n";
        my $formatline=sub {
            my $line = shift;
            my $end  = shift;
            my $len  = shift || 78;
            $len -= length $line;
            return $line.sprintf('%*s', $len, $end)."\n";
        };
        $r.=$formatline->("CONTENTS", "*$addon-contents*");
        my $space = (" " x (78-30-16-(length $addon)));
        $r.=<<"EOF";
    1. Intro                  $space|$addon-intro|
    2. Functionality provided $space|$addon-functionality|
        2.1. Commands         $space|$addon-commands|
        2.2. Functions        $space|$addon-functions|
        2.3. Variables        $space|$addon-variables|
    3. Options                $space|$addon-options|

$ruler
EOF
        $r.=$formatline->("1. Intro", "*$addon-intro*");
        my @dependencies=listDependencies($infohash);
        if(scalar @dependencies) {
            $r.="\nPlugin requires some additional plugins:\n".
                (join("\n", (map {"  - $_"} @dependencies)))."\n".
                "(with their dependencies).";
        }
        $r.="\n$ruler\n".
            $formatline->("2. Functionality provided","*$addon-functionality*").
            "\n\n$ruler2\n".
            $formatline->("2.1. Commands",            "*$addon-commands*").
            "\n\n$ruler2\n".
            $formatline->("2.2. Functions",           "*$addon-functions*").
            "\n\n$ruler2\n".
            $formatline->("2.3. Variables",           "*$addon-variables*").
            "\n\n$ruler\n".
            $formatline->("3. Options",               "*$addon-options*").
            "\n\n\nvim: ft=help:tw=78";
        return ($fname, $r);
    },
    plugin => sub {
        my $addon    = shift;
        my $fname    = "plugin/$addon.vim";
        my %options  = @_;
        my $oprefix  = $options{"oprefix"} || $addon;
        $oprefix=~s/[^a-zA-Z0-9_]//g;
        $oprefix=~s/^\d+//;
        my $ovar     = "g:$oprefix"."Options";
        my $cprefix  = $options{"cprefix"} || $oprefix;
        $cprefix=~s/^(\u)/\U$1/;
        my $fprefix  = $options{"fprefix"} || $cprefix;
        my $r=<<"EOF";
"▶1
scriptencoding utf-8
if !exists('s:_pluginloaded')
    execute frawor#Setup('0.0', {}, 0)
    finish
elseif s:_pluginloaded
    finish
endif
"▶1
call frawor#Lockvar(s:, '')
" vim: ft=vim ts=4 sts=4 et fmr=▶,▲
EOF
        return ($fname, $r);
    },
    info => sub {
        my $addon    = shift;
        my $fname    = "$addon-addon-info.txt";
        my $description = shift;
        if(defined $description) {
            $description=~s/(\\")/\\$1/g;
            $description="\"$description\"";
        }
        else {
            $description="%-DESCRIPTION-%";
        }
        my $r=<<"EOF";
{
    "name": "$addon",
    "version": "0.0",
    "author": "ZyX <kp-pav\@yandex.ru>",
    "maintainer": "ZyX <kp-pav\@yandex.ru>",
    "description": $description,
    "repository": {
        "type": "hg",
        "url": "https://bitbucket.org/ZyX_I/$addon",
    },
    "dependencies": {
        "frawor": {
            "type": "hg",
            "url": "https://bitbucket.org/ZyX_I/frawor",
        }
    },
}
EOF
        return ($fname, $r);
    },
);
#▶2 ParseHelp
sub ParseHelp($) {
    my $helpfile=shift;
    my $HELP;
    open $HELP, "<:utf8", $helpfile;
    my @rlines=();
    my %helptags;
    local $_;
    my $line=1;
    my $summary;
    foreach (<$HELP>) {
        $summary=$1 if($line==1 and /^\*[^*]+\*\ (.*)/);
        if(/^1\. Intro/../^={10}/) {
            push @rlines, $_;
        }
        for my $tag (/(?<=\*)\S+(?=\*)/g) {
            $helptags{$tag}=$line;
        }
        $line++;
    }
    pop @rlines;
    pop @rlines;
    shift @rlines;
    return ($summary, \@rlines, \%helptags);
}
#▶2 SetREADME :: addon, file, readme, helptags → + FS
sub SetREADME($$$$) {
    my $addon  = shift;
    my $file   = shift;
    my $readme = shift;
    my $htags  = shift;
    my @rlines=map {
            local $_=$_;
            $_=Encode::decode_utf8($_) unless Encode::is_utf8($_);
            $_.="\n" unless(/( |^)$/);
            s@([\w\-]+) \((https?://[^)]+)\)@[$1]($2)@g;
            s@([\w\-]+) \(vimscript #(\d+)\)@[$1](http://www.vim.org/scripts/script.php?script_id=$2)@g;
            s/\|([^| ]+)\|/defined $htags->{$1} ?
                            ("[".$1."](".$docbase."#".
                                               URI::Escape::uri_escape($1).")"):
                            "$1"/ge;
            $_;
        } @$readme;
    push @rlines,"Documentation is available online at [$docbase]($docbase).\n";
    if(-r "$addroot/doc/$addon.rux") {
        (my $rudocbase=$docbase)=~s/\.txt\.html$/\.rux\.html/;
        push @rlines, "Russian documentation is at [$rudocbase]($rudocbase).\n";
    }
    my $README;
    open $README, ">:utf8", $file;
    print $README join "", @rlines;
}
#▶2 GetString :: () + user input → s
sub GetString() {
    my $t=File::Temp->newdir();
    $ENV{"t"}=$t->dirname."/file";
    system vim => $ENV{"t"}
        and die "Vim failed.";
    my $T;
    open $T, '<:utf8', $ENV{"t"}
        or die "Failed to open $ENV{'t'}";
    my $r = join "", <$T>;
    undef $t;
    return $r;
}
#▶2 SCM
#▶3 Commit :: repository, comment[, files] -> _
sub Commit($$;@) {
    my $reporoot=shift;
    my $comment=shift;
    my @files=@_;
    system hg => commit => @files, "-R", $reporoot,
                                   "-m", $comment;
}
#▶3 Push :: repository -> _
sub Push($@) {
    system hg => push => "-R", shift @_, "-r", "default", @_;
}
#▶3 Tag :: repository, tag -> _
sub Tag($$) {
    my $reporoot=shift;
    my $tag=shift;
    system hg => tag => $tag, "-R", $reporoot, "-r", "default";
    system hg => rollback =>  "-R", $reporoot;
}
#▶3 CopyFiles :: repository, revision, destination -> _
sub CopyFiles($$$) {
    my $reporoot=shift;
    my $revision=shift;
    my $destdir=shift;
    system hg => archive => "-R", $reporoot,
                            "-r", $revision,
                            "-t", "files",
                            $destdir;
}
#▶3 TarArchive :: repository, revision, destination -> _
sub TarArchive($$$) {
    my $reporoot=shift;
    my $revision=shift;
    my $destdir=shift;
    system hg => archive => "-R", $reporoot,
                            "-r", $revision,
                            "-t", "tar",
                            $destdir;
}
#▶3 AddFile :: repository, files -> _
sub AddFile($@) {
    my $reporoot=shift;
    system hg => add => "-R", $reporoot, @_;
}
#▶3 ListTags :: repository -> [tags]
sub ListTags($) {
    my $reporoot=shift;
    my @tags=();
    my $HG;
    open $HG, '-|', qw(hg tags -R) => $reporoot;
    local $_;
    while(<$HG>) {
        push @tags, $1
            if(/^(\S+) /);
    }
    return @tags;
}
#▶1 Действия
#▶2 list-all-dependencies: print NL-separated list of dependencies
if($action eq "list-all-dependencies") {
    print join "\n", listAllDependencies($addon);
}
#▶2 add-template-{what}: add template doc
elsif($action =~ s/^add-template-//) {
    my ($fname, $text)=$templates{$action}->($addon, @ARGV);
    my $F;
    open $F, ">", "$addroot/$fname";
    print $F $text;
}
#▶2 get-{field}: print value | NL-separated list of values|keys
elsif($action=~s/^get-//) {
    my $infohash=getAddonInfo($addon);
    my $result=$infohash->{$action};
    if(not ref $result) {
        print $result;
    }
    elsif(ref $result eq "ARRAY") {
        print join "\n", @$result;
    }
    elsif(ref $result eq "HASH") {
        print join "\n", keys %$result;
    }
}
#▶2 create-[live-]ebuild: Create a ebuild and print it to 3rd arg (or STDOUT)
elsif($action=~s/^create-//) {
    my $file=shift @ARGV;
    my $FN;
    if($file) {
        open $FN, '>', $file
            or die "Failed to open $file for writing";
    }
    else {
        $FN=*STDOUT;
    }
    if($action eq "live-ebuild") {
        print $FN prepareEbuild($addon, "9999");
    }
    elsif($action eq "ebuild") {
        print $FN prepareEbuild($addon);
    }
    close $FN;
}
#▶2 make-live-ebuilds: make live ebuilds for addon and all its dependencies
elsif($action eq "make-live-ebuilds") {
    my $infohash=getAddonInfo($addon);
    for my $a ($infohash, listAllDependencies($infohash)) {
        makeEbuild($a, "9999");
    }
    Push($reporoot);
}
#▶2 update-{field}: set field value to 3rd arg
elsif($action=~s/^update-//) {
    my $value=shift @ARGV
        or die "Value required";
    updateField($addon, $action, $value);
}
#▶2 post: post addon's file (3rd) to vim.org with comment (4) and vimver (5)
elsif($action eq "post") {
    cleanTMP();
    my $file=shift @ARGV
        or makeArchive($addon);
    my $vercom=shift @ARGV
        or die "Version comment required";
    my $vimver=shift @ARGV;
    postScript($addon, $file, $vercom, $vimver);
}
#▶2 remove-archive: remove archive that is already posted on vim.org
elsif($action eq "remove-archive") {
    removeScript($addon, shift @ARGV);
}
#▶2 release: release a new version (3rd) with comment (4th) and vimver (5th)
elsif($action eq "release") {
    my $version = shift @ARGV
        or die "Version required";
    my $vercom  = shift @ARGV;
    my $vimver  = shift @ARGV;
    if($vercom =~ /^\d+(?:\.\d+)*$/) {
        $vimver = $vercom;
        $vercom = undef;
    }
    $vercom=GetString() unless $vercom;
    if($vercom =~ /^vimorg-tagged:\n/) {
        $vercom =~ s/^vimorg-tagged:\n//;
        $vercom = ReformatAsVOTagged($vercom);
    }
    print "$vercom\n";
    print "Press 'y' to continue:";
    Term::ReadKey::ReadMode 'raw';
    die "Pressed not 'y'" unless(Term::ReadKey::ReadKey(0) eq 'y');
    Term::ReadKey::ReadMode 'restore';
    print "\n";
    my $infohash=getAddonInfo($addon);
    my $oldversion=$infohash->{"version"};
    $version=setNewVersion($version, $oldversion);
    # die "New version must be greater then old"
    my $isnewver=(compareVersions($version, $oldversion)==1);
    if($isnewver) {
        updateField($addon, "version", $version);
        Tag($addroot, "release-$version");
        Commit($addroot, "Updated package version, added tag",
               $addinfo, "$addroot/.hgtags");
    }
    File::Path::remove_tree($tmpdir);
    File::Path::mkpath("$tmpdir/addons")
        or die "Failed to create addons directory";
    makeEbuild($addon, $version);
    my $arch=makeArchive($addon, $version);
    Push($reporoot);
    Push($reporoot, "ssh://hg\@bitbucket.org/ZyX_I/pluginloader-overlay");
    Push($addroot);
    postScript($addon, $arch, $vercom, $vimver);
    my ($summary, $readme, $helptags)=ParseHelp($defhelpfile);
    if($infohash->{"use_help_for_vimorg"}) {
        updateScriptPage($addon, $summary, $readme, undef, $helptags);
    }
    my $chcom="Syncronized page contents with plugin help %s(done by pkgdo.pl)";
    if($isnewver) {
        $chcom=sprintf $chcom, "on release $version "; }
    else {
        $chcom=sprintf $chcom, "on rerelease of version $version "; }
    updateWikis($addon, $summary, $readme, $chcom, $helptags);
    if(-x "$addroot/samples/generate.zsh") {
        system "$addroot/samples/generate.zsh", undef;
    }
}
#▶2 repost: recreate archives for addons that depend on (2nd) and post them to 
#           vim.org
elsif($action eq "repost") {
    my $comment=shift @ARGV || "Reposting bundle due to core plugins update";
    my $vimver="9999";
    for my $p (listPlugins) {
        next unless($p eq $addon
                    or grep {$_ eq $addon} listAllDependencies($p));
        print "Reposting $p\n";
        cleanTMP();
        my $arch=makeArchive($p);
        postScript($p, $arch, $comment, $vimver);
    }
}
#▶2 prepare-test-enviroment: copy all dependencies into temp directory
elsif($action eq "prepare-test-enviroment") {
    my $uselatest = shift @ARGV;
    my $infohash  = getAddonInfo($addon);
    my $rtp="$tmpdir/rtp";
    cleanTMP();
    mkdir $rtp;
    makeBundleDirectory($infohash, $rtp, 1, $uselatest);
    if(defined $ENV{"TESTREV"} and $ENV{"TESTREV"} ne ".") {
        CopyFiles("$devroot/$addon", $ENV{"TESTREV"}, $rtp);
    }
    else {
        -d $rtp or File::Path::mkpath($rtp);
        system cp => "-r", lsdir("$devroot/$addon", 1), $rtp;
    }
    system mv => "$rtp/test" => $tmpdir;
    my $ttdir="$tmpdir/test/".$ENV{"TESTTYPE"};
    system mv => "-f", File::Glob::bsd_glob("$ttdir/*"), "$tmpdir/test"
        if -d "$ttdir";
    cleanTMP('rtp/{.hg*,*-addon-info.txt,addon-info.json}');
    my $VIMRC;
    open $VIMRC, ">", "$tmpdir/vimrc";
    local $_=$ENV{"TESTRTP"} || $rtp;
    s/([\\"])/\\$1/g;
    s/\n/\\n/g;
    print $VIMRC "set nocompatible nomore viminfo= guioptions=cfM noswapfile\n";
    print $VIMRC "set nowritebackup undolevels=-1 encoding=utf-8\n";
    print $VIMRC "set fileformats=unix,dos,mac\n";
    print $VIMRC "let &shelltemp=!has('filterpipe')\n";
    print $VIMRC "let &rtp=\"$_\"\n";
    print $VIMRC "let g:outfile=g:curtest.'.out'\n";
    if($ENV{"TESTTYPE"} =~ /wine/) {
        print $VIMRC "set guifont=Courier_New:h10:cRUSSIAN\n";
        print $VIMRC "set shell=cmd.exe\n";
        print $VIMRC "set shellquote=\\\"\n";
        print $VIMRC "set shellxquote=\n";
        print $VIMRC "set shellcmdflag=/s\\ /c\n";
    }
    if(-e "$tmpdir/test/vimrc") {
        my $TESTVIMRC;
        open $TESTVIMRC, "<", "$tmpdir/test/vimrc";
        print $VIMRC $_ while(<$TESTVIMRC>);
        unlink "$tmpdir/test/vimrc";
    }
    close $VIMRC;
}
#▶2 help-update: generate readme file, description from help file, …
elsif($action eq "help-update") {
    my $helpfile = shift @ARGV || $defhelpfile;
    my $readtgt  = shift @ARGV || "$addroot/README.markdown";
    my ($summary, $readme, $helptags)=ParseHelp($helpfile);
    updateField($addon, "description", $summary);
    updateRepoSummary($addon, $summary);
    SetREADME($addon, $readtgt, $readme, $helptags);
}
#▶2 set-vimorg
elsif($action eq "set-vimorg") {
    my $summary  = shift @ARGV;
    my $readme   = shift @ARGV;
    my $idetails = shift @ARGV;
    my ($helpsummary, $helpreadme, $helptags)=ParseHelp($defhelpfile);
    $summary = $helpsummary unless $summary;
    $readme  = $helpreadme  unless $readme;
    updateScriptPage($addon, $summary, $readme, $idetails, $helptags);
}
#▶2 print-readme, print-summary
elsif($action =~ /^print-(summary|(script-)?readme)$/) {
    my ($helpsummary, $helpreadme, $helptags)=ParseHelp($defhelpfile);
    if($1 eq "summary") {
        print $helpsummary;
    }
    else {
        if($2) {
            print formatScriptReadme($addon, $helpreadme, $helptags, 0);
        }
        else {
            print join "", @$helpreadme;
        }
    }
}
#▶2 reset-wikis
elsif($action eq "reset-wikis") {
    my $infohash=getAddonInfo($addon);
    my $uchcom=shift @ARGV;
    my $chcom="Syncronized page contents with plugin help %s(done by pkgdo.pl)";
    if(defined $uchcom) {
        $chcom=sprintf $chcom, $uchcom; }
    else {
        $chcom=sprintf $chcom, ""; }
    my ($summary, $readme, $helptags)=ParseHelp($defhelpfile);
    updateWikis($addon, $summary, $readme, $chcom, $helptags);
}
#▶2 set-{type}-wiki
elsif($action =~ s/^set-(.*?)-wiki$/$1/) {
    die "Unknown wiki type: $action" unless defined $wpupdfuncs{$action};
    my $url=shift @ARGV;
    die "Undefined url" unless defined $url;
    my $readme=shift @ARGV;
    my $helptags;
    unless(defined $readme) {
        my $summary;
        ($summary, $readme, $helptags)=ParseHelp($defhelpfile);
    }
    $wpupdfuncs{$action}->($addon, $url, $readme, "Changed by pkgdo.pl",
                           $helptags);
}
#▶2 TODO clean: clean old ebuilds
elsif($action eq "clean") {
    my @versions=listVersions($addon);
    my $edir="$reporoot/app-vim/$addon";
    for my $v (@versions) {
        my $p="$addon-$v";
    }
    # my $efile="$edir/$addon-$version.ebuild";
}
#▶2 TODO format-help
elsif($action eq "format-help") {
    my @helpfiles=File::Glob::bsd_glob("$devroot/*/doc/*.{txt,??x}");
    cleanTMP();
    my $docdir="$tmpdir/doc";
    mkdir $docdir;
    map {File::Copy::copy($_, $docdir)} @helpfiles;
    my $escapeddocdir=fnameescape($docdir);
    foreach (@helpfiles) {
        my ($language, $tags)=("en", "./tags");
        if(/(..)x$/) {
            $language=$1;
            $tags="./tags-$language,./tags";
        }
        my $helpfile=fnameescape($_);
        system view => "-c", "let f=LoadFuncdict('format')",
                       "-c", "set nolist helplang=$language tags=$tags",
                       "-c", "let g:formatOptions.ShowProgress=1",
                       "-c", "let g:formatOptions.IgnoreTags=0",
                       "-c", "call writefile(f.format('html'), $helpfile)",
                       "-c", "qa!",
                       $_;
    }
    cleanTMP("doc/*.{txt,??x}");
}
#▶2 help
elsif($action eq "help") {
    my $topic=$addon;
    unless(defined $topic) {
        print <<"EOF";
Usage:
    $0 list-all-dependencies {addon}
    $0 add-template-{what} {addon}
    $0 get-{field} {addon}
    $0 create-live-ebuild {addon} [{file}]
    $0 make-live-ebuilds {addon}
    $0 update-{field} {addon} {new-value}
    $0 post {addon} {file} [{version-comment}] [{vim-version}]
    $0 remove-archive {addon} [{archive-name-regex}]
    $0 release {addon} {version} {version-comment} [{vim-version}]
    $0 repost {addon} [{message}]
    $0 prepare-test-enviroment {addon} [{uselatest}]
    $0 help-update {addon} [{helpfile} [{readmefile}]]
    $0 set-vimorg {addon} [{summary} [{readme} [{install-details}]]]
    $0 reset-wikis {addon} [{uchcom}]
    $0 set-{wiki}-wiki {addon} {url} {text}
    $0 print-[script-]readme {addon}
    $0 print-summary {addon}
EOF
    }
    else {
        die "Not implemented";
    }
}
#▲2

# vim: ft=perl:ts=4:et:tw=80:sts=4:fmr=#▶,#▲
