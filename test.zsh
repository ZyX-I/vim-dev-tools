#!/bin/zsh
#▶1 setopt
emulate -L zsh
setopt extendedglob
setopt nullglob
#▶1 Define variables
typeset -xr STUFDIR=$0:A:h
typeset -x  DEVDIR=$STUFDIR:h
typeset -x  TESTREV=.
typeset -x  TESTTYPE=vim
typeset -x  VIMPATH=/usr/bin
typeset -ga ADDARGS
typeset -g  CT ADDON
typeset -gi FOREGROUND=-1
typeset -gi STOPJOBS=0
typeset -gi USELATEST=0
typeset -g  VIRTENV=""
typeset -gi PARSEOPTS=0
typeset -ga VALGRIND
typeset -ga SCREEN
typeset -gi USESCREEN
typeset -ga ADDGLOBS
#▶1 help()
typeset -gr P=$0:t
function help() {
local s=${P//?/ }
<< EOF
Usage: $P [ -b | -f | -s | -l ] [ -r revision ] [ -t testtype ] [ -p vimpath ]
       $s [ -a addon ] [ -T testname ] [ -d devdir ] [ -v virtenv ] [ -V cmd ]
       $s [ -S cmd ] [ -F glob ] [ -- vimoptions ]

    -b       Run test in background (default unless -T was specified)
    -f       Run test in foreground (default if -T was specified)
    -s       Stop after first failed test
    -l       Use latest (development) versions of all dependencies
    -r rev   Test specific revision
    -T name  Run only specific test
    -t ttype Run tests for different vim. Supported test types:
                vim     Test console vim (default)
                gvim    Test GUI vim
                wine    Test console vim in wine
                gwine   Test GUI vim in wine
    -p path  Use different path to vim executable (not for *wine tests)
    -a addon Test specific addon (file {devdir}/{addon}/{addon}-addon-info.txt
             or {devdir}/{addon}/addon-info.json must exist where {devdir} is
             determined by -d switch)
    -d path  Search addons in given path (default: two levels up relative to
             directory where $P is located)
    -v name  Work in given virtual environment
    -V cmd   Run vim inside another app (e.g. valgrind, strace): the one that 
             accepts being launched like “\$CMD {cmd opts} vim {vim opts}”
    -S cmd   Run vim inside another app in place of screen. {cmd} is handled 
             like above, but with -V vim is launched in screen pseudo-terminal, 
             with -S screen is replaced with {cmd}.
    -F glob  Copy files matching glob from plugin testing directory to testing 
             directory. Useful in conjunction with -r to run tests that were not 
             committed or to run recent version of test on previous commit

    Note: -V and -S do not work with “-t wine” and “-t gwine”. With “-t gvim” 
          screen is not used by default.
EOF
}
#▶1 Parse options
while (( $# )) ; do
    case $1 in
        -h) help           ; exit  ;;
        -b) FOREGROUND=0   ; shift ;;
        -f) FOREGROUND=1   ; shift ;;
        -s) STOPJOBS=1     ; shift ;;
        -l) USELATEST=1    ; shift ;;
        -=) PARSEOPTS=1    ; shift ;;
        -r) TESTREV=$2     ;|
        -t) TESTTYPE=$2    ;|
        -p) VIMPATH=$2     ;|
        -a) ADDON=$2       ;|
        -T) CT=$2          ;|
        -d) DEVDIR=$2      ;|
        -v) VIRTENV=$2     ;|
        -F) ADDGLOBS+=($2) ;|
        -V) eval "VALGRIND=( $2 )" ;|
        -S) eval "SCREEN=( $2 )"   ; USESCREEN=1 ;|
        --) shift ; break ;;
        -*) shift ; shift ;;
         *) break ;;
    esac
done
#▶2 ADDON option
if [[ -z $ADDON ]] ; then
    if (($#)) && [[ -e $DEVDIR/$1 ]] ; then
        ADDON=$1
        shift
    else
        local DIR=$PWD
        local PREVDIR=""
        while [[ ! -e $DIR/$DIR:t-addon-info.txt ]] && [[ ! -e $DIR/addon-info.json ]] && [[ $DIR != $PREVDIR ]]
        do
            PREVDIR=$DIR
            DIR=$DIR:h
        done
        ADDON=$DIR:t
    fi
fi
#▶2 CT and ADDARGS
if (($#)) ; then
    if [[ -z $CT ]] ; then
        CT=$1
        shift
    fi
    set -A ADDARGS $@
fi
#▲2
(( FOREGROUND==-1 )) && (( FOREGROUND=!!$#CT ))
#▶1 Do some checks
[[ -e $DEVDIR/$ADDON/${ADDON}-addon-info.txt ]] || [[ -e $DEVDIR/$ADDON/addon-info.json ]] || exit 2
[[ -x $VIMPATH/vim                           ]] || exit 3
#▶1 PARSEOPTS support
if (( PARSEOPTS )) ; then
    typeset -ga RESTARGS
    RESTARGS=( $@ )
    typeset TESTREV TESTTYPE VIMPATH ADDARGS CT ADDON FOREGROUND STOPJOBS \
            VIRTENV DEVDIR RESTARGS
    exit 0
fi
#▶1 Get TMPDIR and all dependants
. $STUFDIR/gettemp.zsh ${ADDON}-tests
typeset -xr TMPDIR=${TMPDIR}/t
typeset -gr RTP=$TMPDIR/rtp
typeset -xr TESTDIR=$TMPDIR/test
#▶1 Make variables read-only and export them
typeset -gr  CT       ADDON      VIRTENV
typeset -xr  TESTTYPE TESTREV    VIMPATH   DEVDIR
typeset -gir STOPJOBS FOREGROUND USELATEST USESCREEN
typeset -gar VALGRIND SCREEN     ADDARGS   ADDGLOBS
#▶1 vimcmd(), ISWINE
typeset -gi ISWINE=0
case $TESTTYPE in
    vim)
        function vimcmd()
        {
            local -a SOPTS
            if (( $USESCREEN )) ; then
                $SCREEN $VALGRIND $VIMPATH/vim $@
            else
                (( FOREGROUND )) && SOPTS=(    -m ) \
                                 || SOPTS=( -D -m )
                (( $+SESNAME )) && SOPTS+=( -S $SESNAME )
                screen -L -c =(echo "logfile $LOGFILE") $SOPTS $VALGRIND $VIMPATH/vim $@
            fi
        }
        ;;
    gvim)
        function vimcmd()
        {
            $SCREEN $VALGRIND $VIMPATH/vim -gf -u ${TMPDIR}/vimrc $@
        }
        ;;
    wine)
        ISWINE=1
        function vimcmd()
        {
            local -x TESTDIR='T:\test'
            local -x LC_TIME='ru_RU.CP1251'
            local    WINECMD=wine
            (( FOREGROUND )) && WINECMD=wineconsole
            $WINECMD 'C:\vim\vim.exe' $@ &>$LOGFILE
        }
        ;;
    gwine)
        ISWINE=1
        function vimcmd()
        {
            local -x TESTDIR='T:\test'
            local -x LC_TIME='ru_RU.CP1251'
            wine 'C:\vim\gvim.exe' $@ &>$LOGFILE
        }
        ;;
esac
typeset -gir ISWINE
#▶1 VIMRUNTIME
if (( ISWINE )) ; then
    typeset -xr VIMRUNTIME='T:\rtp'
else
    typeset -xr VIMRUNTIME=$RTP
fi
#▶1 fail()
typeset -gir KILLPID=$$
function fail()
{
    echo "$1 $2" >> $TMPDIR/failed.lst
    (( STOPJOBS )) && kill -USR2 $KILLPID
}
#▶1 dotest()
function dotest()
{
    local -r T=$1
    local -r CT=${T:t:r}
    shift
    test -x $CT.pre && CURTEST=$CT ./$CT.pre
    LOGFILE=$CT.log     \
    SESNAME=vim-test-${ADDON}-$CURJOB \
    vimcmd -u ${VIMRC} --cmd "let g:curtest=${(qqq)CT}" -s $T $@
    [[ -e $TESTDIR/$CT.fail ]] && \
        fail f $CT
    if [[ -e $TESTDIR/$CT.ok ]] ; then
        if [[ ! -e $TESTDIR/$CT.out ]] ; then
            fail m $CT
        elif ! diff -q $TESTDIR/$CT.{ok,out} ; then
            fail o $CT
        fi
    fi
}
#▶1 TESTRTP for wine tests
if (( ISWINE )) ; then
    export TESTRTP='T:\rtp'
fi
#▲1
#▶1 Prepare test environment
${STUFDIR}/pkgdo.pl prepare-test-enviroment $ADDON $USELATEST || exit 128
typeset -ga  ADDFILES
ADDFILES=( $DEVDIR/$ADDON/test/$^~ADDGLOBS  )
typeset -gar ADDFILES
if ((#ADDFILES)) ; then
    cp $ADDFILES $TESTDIR
fi
#▶1 VIMRC, some wine setup
if (( ISWINE )) ; then
    if [[ -e ~/.wine/dosdevices/t: ]] ; then
        mv ~/.wine/dosdevices/t: $TMPDIR
    fi
    ln -s $TMPDIR:a ~/.wine/dosdevices/t:
    typeset -gr VIMRC='T:\vimrc'
else
    typeset -gr VIMRC="${TMPDIR}/vimrc"
fi
#▶1 parjobs setup
. ${STUFDIR}/parjobs.zsh
function printjob()
{
    echo -n ${${${(P)1}[-1-$#ADDARGS]}:t:r}
}
test -x $TESTDIR/parjobssetup.zsh && . $TESTDIR/parjobssetup.zsh
#▲1
cd $TESTDIR
#▶1 setup runtimepath from runtime.tar.xz
if test -r $TESTDIR/runtime.tar.xz ; then
    if ! cat $TESTDIR/runtime.tar.xz | unxz | tar x -C $RTP ; then
        exit 5
    fi
    rm $TESTDIR/runtime.tar.xz
fi
#▶1 generate tests
for gentest in $TESTDIR/gentests* ; do
    if [[ -x $gentest ]] ; then
        if [[ $gentest[-4,-1] == '.zsh' ]] ; then
            ( . $gentest )
        else
            $gentest
        fi
    fi
done
[[ -n $CT && ! -r $TESTDIR/$CT.in ]] && exit 4
#▶1 process musthave.dat file
if [[ -e $TESTDIR/musthave.dat ]] ; then
    . ${STUFDIR}/checkhas.zsh
fi
#▶1 appendexit
function appendexit()
{
    cat $1 <(echo -n $'\C-\\\C-n:silent! qa!\n') > $1.q
    rm -f $1
    mv $1.q $1
}
#▶1 virtualenv
if [[ ! -z $VIRTENV ]] ; then
    . virtualenvwrapper.sh
    workon $VIRTENV
    typeset -xr VERTP=$TMPDIR/virtualenv
    git clone --depth=1 git://github.com/jmcantrell/vim-virtualenv $VERTP
    export VIRTENV
    ADDARGS=( --cmd $'set rtp=$VERTP
                      runtime! plugin/*.vim
                      call virtualenv#activate($VIRTENV)' )
fi
typeset -gar ADDARGS
#▲1
if [[ -z $CT ]] ; then
    for testfile in $TESTDIR/*.in
    do
        if (( FOREGROUND )) ; then
            dotest $testfile $ADDARGS
        else
            appendexit $testfile
            addjob dotest $testfile $ADDARGS
        fi
    done
    (( FOREGROUND )) || waitjobs
else
    testfile=$TESTDIR/$CT.in
    (( FOREGROUND )) || appendexit $testfile
    dotest $testfile $ADDARGS
fi
#▶1 Restore t: symlink
if (( ISWINE )) ; then
    rm ~/.wine/dosdevices/t:
    if [[ -e $TMPDIR/t: ]] ; then
        mv $TMPDIR/t: ~/.wine/dosdevices
    fi
fi
#▲1

! cat $TMPDIR/failed.lst 2>/dev/null
# vim: ft=zsh:fenc=utf-8:ts=4:fdm=marker:fmr=▶,▲:cms=#%s
