#!/usr/bin/perl
use strict;
use warnings;
use utf8;

use File::Basename;
use Cwd;

exit 0 if(`hg branch` ne "default\n");

exit 0 unless($ENV{"FORCE_README"} or `hg status doc`);

my $stufroot = $ENV{"STUFDIR"} || File::Basename::dirname(Cwd::realpath($0));
exit 0 if system "$stufroot/format-help.zsh" and not $ENV{"FORCE_README"};

system "$stufroot/pkgdo.pl", "help-update", "." if($ARGV[0] eq "README");
system "$stufroot/pkgdo.pl", "reset-wikis", ".",
                             "on commit ".`hg log -r . --template {node}`." "
                             if($ARGV[0] eq "WIKI");
